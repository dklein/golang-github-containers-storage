golang-github-containers-storage (1.21.2+dfsg1-1) unstable; urgency=medium

  * New upstream version 1.21.2+dfsg1
  * debian/control: depend on golang-github-opencontainers-specs-dev
  * drop patches, applied upstream
  * Provide existant $HOME for tests
  * debian/control: depend on golang-github-golang-github-hashicorp-go-multierror-dev

 -- Reinhard Tartler <siretart@tauware.de>  Fri, 24 Jul 2020 06:40:48 -0400

golang-github-containers-storage (1.20.2-3) unstable; urgency=medium

  * Upload to unstable

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 20 Jul 2020 06:45:35 -0400

golang-github-containers-storage (1.20.2-2) experimental; urgency=medium

  * unbreak build on mipsen

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 19 Jul 2020 15:27:37 -0400

golang-github-containers-storage (1.20.2-1) experimental; urgency=medium

  * New upstream version: 1.20.2
  * remove local patches, all merged upstream
  * Exclude tests in pkg/unshare

 -- Reinhard Tartler <siretart@tauware.de>  Thu, 09 Jul 2020 21:00:38 -0400

golang-github-containers-storage (1.18.2+dfsg1-1) experimental; urgency=medium

  * New upstream version 1.18.2+dfsg1
  * pick up new dependencies

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 19 Apr 2020 18:12:56 -0400

golang-github-containers-storage (1.15.8+dfsg1-2) unstable; urgency=medium

  * Add patch to fix FTBFS on mipsen

 -- Reinhard Tartler <siretart@tauware.de>  Fri, 29 May 2020 06:44:22 -0400

golang-github-containers-storage (1.15.8+dfsg1-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version: 4.5.0.

 -- Dmitry Smirnov <onlyjob@debian.org>  Wed, 05 Feb 2020 11:05:13 +1100

golang-github-containers-storage (1.15.7+dfsg1-1) unstable; urgency=medium

  * New upstream release.
  * control: set "Rules-Requires-Root: no".

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 26 Jan 2020 20:40:19 +1100

golang-github-containers-storage (1.15.5+dfsg1-1) unstable; urgency=medium

  * New upstream release.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 29 Dec 2019 22:57:42 +1100

golang-github-containers-storage (1.15.3+dfsg1-1) unstable; urgency=medium

  * New upstream release.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 15 Dec 2019 14:51:36 +1100

golang-github-containers-storage (1.15.2+dfsg1-1) unstable; urgency=medium

  * New upstream release.

 -- Dmitry Smirnov <onlyjob@debian.org>  Tue, 10 Dec 2019 19:06:27 +1100

golang-github-containers-storage (1.15.1+dfsg1-1) unstable; urgency=medium

  * New upstream release.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 01 Dec 2019 03:17:29 +1100

golang-github-containers-storage (1.13.5+dfsg1-1) unstable; urgency=medium

  * New upstream release.
  * Install "containers-storage.conf.5" man page.
  * Install example "storage.conf".
  * Updated "watch" file to fix L/W: debian-watch-file-should-mangle-version.
  * (Build-)Depends:
    - golang-github-ostreedev-ostree-go-dev
    - libostree-dev

 -- Dmitry Smirnov <onlyjob@debian.org>  Tue, 12 Nov 2019 08:48:35 +1100

golang-github-containers-storage (1.13.2+dfsg1-3) unstable; urgency=medium

  * Build with "ostree" tag.
  * Standards-Version: 4.4.1.
  * Added myself to Uploaders.
  * Switched to the original "ostreedev/ostree-go" library.
  * (Build-)Depends:
    - golang-github-sjoerdsimons-ostree-go-dev
    + golang-github-ostreedev-ostree-go-dev

 -- Dmitry Smirnov <onlyjob@debian.org>  Mon, 11 Nov 2019 13:51:54 +1100

golang-github-containers-storage (1.13.2+dfsg1-2) unstable; urgency=medium

  * Team upload.

  [ Dmitry Smirnov ]
  * New patch to fix FTBFS on mips.

  [ Arnaud Rebillout ]
  * New upstream patch to fix build against docker-dev 19.03.
  * Removed "golang-github-docker-docker-dev" from (Build-)Depends.

 -- Arnaud Rebillout <arnaud.rebillout@collabora.com>  Fri, 01 Nov 2019 11:19:03 +0700

golang-github-containers-storage (1.13.2+dfsg1-1) unstable; urgency=medium

  [ Reinhard Tartler ]
  * Prepare new upload
  * New upstream requires golang-github-datadog-zstd-dev

  [ Dmitry Smirnov ]
  * Added "Built-Using" field to binary package.
  * dev: added missing ostree dependency; new patch to use available fork.

  [ Reinhard Tartler ]
  * New upstream version 1.13.2

 -- Reinhard Tartler <siretart@tauware.de>  Wed, 11 Sep 2019 07:51:52 -0400

golang-github-containers-storage (1.12.2-1) experimental; urgency=medium

  * New upstream version

 -- Reinhard Tartler <siretart@tauware.de>  Wed, 10 Apr 2019 08:12:52 -0400

golang-github-containers-storage (1.5-1) experimental; urgency=medium

  * Initial release (Closes: #921949)

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 10 Feb 2019 08:23:47 -0500
